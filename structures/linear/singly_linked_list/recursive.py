from typing import Optional, Callable, Any

from .structure import Node


def insert_at_tail(head: Optional[Node], data: Any) -> Node:
    if not head:
        return Node(data)

    head.next = insert_at_tail(head.next, data)
    return head


def traverse(head: Optional[Node], visit: Callable):
    if not head:
        return

    visit(head.data)
    return traverse(head.next, visit)


def visit_reversed(head: Optional[Node], visit: Callable):
    if not head:
        return

    visit_reversed(head.next, visit)
    visit(head.data)


def reverse(head: Optional[Node], tail: Optional[Node] = None) \
        -> Optional[Node]:

    if not head:
        return None

    next_node = head.next
    head.next = tail

    if not next_node:
        return head

    return reverse(next_node, head)


def are_equal(head_1: Optional[Node], head_2: Optional[Node]) -> bool:
    if not head_1 and not head_2:
        return True

    if head_1 is None or head_2 is None:
        return False

    equal = are_equal(head_1.next, head_2.next)
    return equal and head_1.data == head_2.data
