from typing import Optional, Callable, Any

from .structure import Node


def insert_at_head(head: Optional[Node], data: Any) -> Node:
    new_node = Node(data)
    if not head:
        return new_node

    new_node.next = head
    return new_node


def insert_at_tail(head: Optional[Node], data: Any) -> Node:
    # Создаем новый элемент
    new_node = Node(data)

    # Если указатель на голову списка не передан,
    # то создаем новый список с одной нодой
    if not head:
        return new_node

    # Иначе проходим весь список до конца
    current = head
    while current.next:
        current = current.next

    # И присваиваем последнему элементу
    # новый элемент следующим
    current.next = new_node
    return head


def insert_at(head: Optional[Node], data: Any, index: int) -> Node:
    # Создаем новую ноду
    new_node = Node(data)

    # Если указатель на начало списка не передан,
    # то создаем новый список
    if not head:
        return new_node

    # Если индекс нулевой, то новую ноду делаем
    # головой списка
    if index == 0:
        new_node.next = head
        return new_node

    # Иначе проходим по всему списку, сохраняя
    # предыдуший элемент до нужного индекса
    current_index = 0
    current_node = head
    prev_node: Optional[Node] = None

    while current_node:
        prev_node = current_node
        current_node = current_node.next
        current_index += 1
        if current_index == index:
            break

    # Если текущий индекс меньше искомого, то мы
    # наткнулись на конец списка раньше и такого
    # индекса просто нет в списке.
    if current_index < index:
        raise IndexError()

    # Найдя нужную позицию, меняем ссылки на элементы.
    # Предыдущий элемент теперь должен указывать на
    # созданную ноду, а новая нода на текущий элемент,
    # который будет следовать за вставленным элементом.
    new_node.next = current_node
    prev_node.next = new_node
    return head


def traverse(head: Optional[Node], visit: Callable):
    if not head:
        return

    current_node = head
    while current_node:
        visit(current_node.data)
        current_node = current_node.next


def delete_at(head: Optional[Node], index: int) -> Optional[Node]:
    if not head:
        return None

    if index == 0:
        return head.next

    current_index = 0
    prev_node = None
    current_node = head

    while current_node:
        prev_node = current_node
        current_node = current_node.next
        current_index += 1
        if current_index == index:
            break

    prev_node.next = current_node.next
    return head


def reverse(head: Optional[Node]):
    if not head:
        return None

    prev = None
    current = head

    while current:
        next_node = current.next
        current.next = prev
        prev = current
        current = next_node

    return prev


def are_equal(head_1: Optional[Node], head_2: Optional[Node]) -> bool:
    if not head_1 and not head_2:
        return True

    if head_1 is None or head_2 is None:
        return False

    current_1 = head_1
    current_2 = head_2

    while current_1 or current_2:
        if not all([current_1, current_2]):
            return False

        if current_1.data != current_2.data:
            return False

        current_1 = current_1.next
        current_2 = current_2.next

    return True
