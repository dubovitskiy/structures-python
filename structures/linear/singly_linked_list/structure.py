from functools import total_ordering
from typing import Any, Union, Optional


@total_ordering
class Container:
    __slots__ = ('key', 'data')

    def __init__(self, key: int, data: Any = None):
        self.key = key
        self.data = data

    def __lt__(self, other: Union['Container', int]):
        if isinstance(other, int):
            return self.key < other
        return self.key < other.key

    def __eq__(self, other: Union['Container', int]):
        if isinstance(other, int):
            return self.key == other
        return self.key == other.key


class Node:
    __slots__ = ('data', 'next')

    def __init__(self, data: Union[Container, int]):
        self.data = data
        self.next: Optional[Node] = None
