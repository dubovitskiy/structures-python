class ListNode:
    __slots__ = ('key', 'next')

    def __init__(self, key):
        self.key = key
        self.next: ListNode = None


class LinkedList:
    __slots__ = ('head', 'length')

    def __init__(self):
        self.head: ListNode = None
        self.length: int = 0

    def __len__(self):
        return self.length

    def __contains__(self, key):
        return self.find(key) is not None

    def insert(self, key):
        node = ListNode(key)

        if self.head is not None:
            node.next = self.head

        self.head = node
        self.length += 1

    def _find_prev(self, key: any):
        elem = self.head
        result = None

        while elem.next:
            if elem.next.key == key:
                result = elem
                break
            elem = elem.next
        return result

    def remove(self, key: any):
        if key not in self:
            return False

        prev = self._find_prev(key)
        node = prev.next
        next_node = node.next
        prev.next = next_node
        return True

    def find(self, key: any):
        if not self.head:
            return None

        elem = self.head
        while elem:
            if elem.key == key:
                return elem

            elem = elem.next

        return None

    @property
    def is_empty(self):
        return self.length == 0
