from structures.linear.stack_array import Stack


class QueueFull(Exception):
    pass


class QueueEmpty(Exception):
    pass


class Queue:
    def __init__(self, size):
        assert size > 0, "Size cannot be less than zero"

        left_stack_size = size // 2
        right_stack_size = size - left_stack_size

        self._items: int = 0
        self._size: int = size
        self._left_stack: Stack = Stack(left_stack_size)
        self._right_stack: Stack = Stack(right_stack_size)

    def __len__(self):
        return self._items

    @property
    def is_full(self) -> bool:
        return self._left_stack.is_full and self._right_stack.is_full

    @property
    def is_empty(self) -> bool:
        return self._left_stack.is_empty and self._right_stack.is_empty

    def enqueue(self, item: any):
        if self.is_full:
            raise QueueFull()

        if self._left_stack.is_full:
            self._move_items()

        self._left_stack.push(item)
        self._items += 1

    def _is_moving_allowed(self) -> bool:
        return not (
            self._left_stack.is_empty
            and self._right_stack.is_full
        )

    def _move_items(self):
        moving_allowed = self._is_moving_allowed()

        while moving_allowed:
            left_value = self._left_stack.pop()
            self._right_stack.push(left_value)
            moving_allowed = self._is_moving_allowed()

    def dequeue(self) -> any:
        if self.is_empty:
            raise QueueEmpty()

        if self._right_stack.is_empty:
            self._move_items()

        self._items -= 1
        return self._right_stack.pop()
