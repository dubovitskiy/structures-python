class Heap:
    def __init__(self):
        self.heap_list: list = []
        self.current_size: int = 0

    def parent_index(self, index: int):
        return int((index - 1) / 2)

    def left_child(self, parent_idx: int):
        return parent_idx * 2 + 1

    def swap(self, a_idx: int, b_idx: int):
        self.heap_list[a_idx], self.heap_list[b_idx] = \
            self.heap_list[b_idx], self.heap_list[a_idx]

    def sift_up(self):
        index = self.current_size - 1
        parent_index = self.parent_index(index)

        while index > 0 and self.heap_list[parent_index] > self.heap_list[index]:
            self.swap(parent_index, index)
            index = parent_index

    def sift_down(self):
        pass

    def insert(self, elem: int):
        self.heap_list.append(elem)
        self.current_size += 1
        self.sift_up()
