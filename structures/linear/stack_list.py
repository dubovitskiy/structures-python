class StackUnderflow(Exception):
    pass


class Node:
    def __init__(self, key):
        self.key = key
        self.next: Node = None

    def __repr__(self):
        return '<Node key={0}>'.format(self.key)


class Stack:
    def __init__(self):
        self.head: Node = None
        self.length: int = 0

    def __len__(self):
        return self.length

    def __repr__(self):
        return '<Stack len={0}>'.format(self.length)

    @property
    def is_empty(self) -> bool:
        return self.length == 0

    @property
    def is_full(self) -> bool:
        return False

    def push(self, item: any):
        node = Node(item)
        node.next = self.head
        self.head = node
        self.length += 1

    def peek(self) -> any:
        if not self.head:
            raise StackUnderflow()

        # Return current head key
        return self.head.key

    def pop(self) -> any:
        if not self.head:
            raise StackUnderflow()

        result = self.head.key
        self.head = self.head.next
        self.length -= 1
        return result
