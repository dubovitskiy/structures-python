import typing


class StackUnderflow(Exception):
    pass


class StackOverflow(Exception):
    pass


class Stack:
    def __init__(self, size: int):
        self._size: int = size
        self._length: int = 0
        self._items: typing.List[any] = []

    def __len__(self):
        return self._length

    @property
    def size(self) -> int:
        return self._size

    @property
    def is_full(self) -> bool:
        return self._length == self._size

    @property
    def is_empty(self) -> bool:
        return self._length == 0

    def push(self, item: any):
        if self.is_full:
            raise StackOverflow

        self._items.append(item)
        self._length += 1

    def pop(self) -> any:
        if self.is_empty:
            raise StackUnderflow

        self._length -= 1

        return self._items.pop()

    def peek(self) -> any:
        return self._items[-1]
