class Node:
    """Binary search tree node."""
    __slots__ = ('key', 'left', 'right')

    def __init__(self, key: any):
        self.key = key
        self.left: Node = None
        self.right: Node = None

    def __repr__(self):
        return f'<Node(key={self.key})>'

    def __eq__(self, other_node: 'Node') -> bool:
        return self.key == other_node.key

    def __lt__(self, other_node: 'Node') -> bool:
        return self.key < other_node.key

    def __gt__(self, other_node: 'Node'):
        return self.key > other_node.key
