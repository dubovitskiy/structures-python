from collections import deque

from typing import Callable

from structures.recursive.bst.structure import Node


def check_is_bst(root: Node):
    if not root:
        return True

    stack = deque([(root, float('-inf'), float('inf'))])
    while stack:
        root, lower, upper = stack.pop()
        if not root:
            continue

        value = root.key
        if value <= lower or value >= upper:
            return False

        stack.append((root.right, value, upper))
        stack.append((root.left, lower, value))

    return True


def check_is_bst_inorder_traversal(root: Node):
    stack = deque()
    inorder = float('-inf')

    while stack or root:
        while root:
            stack.append(root)
            root = root.left

        root = stack.pop()
        if root.key <= inorder:
            return False

        inorder = root.key
        root = root.right
    return True


def compute_min_value(root: Node) -> int:
    """Get a min value."""

    current_node = root

    while current_node.left:
        current_node = current_node.left

    return current_node.key


def compute_max_value(root: Node) -> int:
    """Get a max value."""

    current_node = root

    while current_node.right:
        current_node = current_node.right

    return current_node.key


def visit_inorder(root: Node, visit: Callable):
    current_node = root
    stack = deque()

    while stack or current_node:
        if current_node:
            stack.append(current_node)
            current_node = current_node.left
        else:
            current_node = stack.pop()
            visit(current_node.key)
            current_node = current_node.right


def visit_inorder_reversed(root: Node, visit: Callable):
    current_node = root
    stack = deque()

    while stack or current_node:
        if current_node:
            stack.append(current_node)
            current_node = current_node.right
        else:
            current_node = stack.pop()
            visit(current_node.key)
            current_node = current_node.left


def visit_inorder_without_stack(node: Node, visit: Callable):
    current_node = node

    while current_node:
        if not current_node.left:
            visit(current_node.key)
            current_node = current_node.right
        else:
            # Находим предшественника текущей ноды
            predecessor = current_node.left
            while predecessor.right and predecessor.right != current_node:
                predecessor = predecessor.right

            # Отмечаем текущую ноду как правого потомка для своего
            # предшественника
            if not predecessor.right:
                predecessor.right = current_node
                current_node = current_node.left

            # Отменим изменения, сделанные в if, чтобы
            # восстановить оригинальное дерево, т. е. исправить
            # правого потомка
            else:
                predecessor.right = None
                visit(current_node.key)
                current_node = current_node.right


def visit_preorder(root: Node, visit: Callable):
    if not root:
        return None

    stack = deque([root])

    while stack:
        node = stack.pop()
        visit(node.key)

        if node.right:
            stack.append(node.right)

        if node.left:
            stack.append(node.left)


def has_value(root: Node, key):
    if not root:
        return False

    if root.key == key:
        return True

    current_node = root

    while current_node:
        if current_node.key == key:
            return True

        if key < current_node.key:
            current_node = current_node.left
        else:
            current_node = current_node.right

    return False


