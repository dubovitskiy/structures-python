from typing import Callable

from structures.recursive.bst.structure import Node


def compute_min_value(root: Node) -> int:
    """Get a minimum recursively."""

    # Return the key of the current node if
    # it doesn't have left child
    if not root.left:
        return root.key

    return compute_min_value(root.left)


def compute_max_value(root: Node) -> int:
    """Get a maximum value recursively."""

    if not root.right:
        return root.key

    return compute_max_value(root.right)


def check_is_bst(root: Node, lower=float('-inf'), upper=float('inf')):
    """Check if tree is binary search tree."""

    if not root:
        return True

    value = root.key
    if value <= lower or value >= upper:
        return False

    left_is_bst = check_is_bst(root.left, lower, value)
    right_is_bst = check_is_bst(root.right, value, upper)

    return left_is_bst and right_is_bst


def insert_node(root: Node, key):
    """Insert a new node to the tree."""

    # Return new node if node is None
    if not root:
        return Node(key)

    if key < root.key:
        # Insert to the left subtree
        root.left = insert_node(root.left, key)

    else:
        # Insert to the right subtree
        root.right = insert_node(root.right, key)

    # Return unchanged node
    return root


def count_nodes(root: Node) -> int:
    """Get a size of the tree."""

    if not root:
        return 0

    return count_nodes(root.left) + 1 + count_nodes(root.right)


def compute_max_depth(root: Node) -> int:
    if not root:
        return 0

    # Вычисляем глубину левого и правого поддеревьев
    left_depth = compute_max_depth(root.left)
    right_depth = compute_max_depth(root.right)

    # Возвращаем максимальное значение +1, т. е. текущую ноду
    return max([left_depth, right_depth]) + 1


def has_value(root: Node, key) -> bool:
    """Search for a value."""

    if not root:
        return False

    if root.key == key:
        return True

    if key < root.key:
        return has_value(root.left, key)
    return has_value(root.right, key)


def visit_inorder(root: Node, visit: Callable):
    """Инфиксный обход.
    1. Левое поддерево
    2. Правое поддерево
    3. Элемент

    Применения:
        1. Сортировка
    """
    if not root:
        return

    visit_inorder(root.left, visit)
    visit(root.key)
    visit_inorder(root.right, visit)


def visit_inorder_reversed(root: Node, visit: Callable):
    if not root:
        return

    visit_inorder_reversed(root.right, visit)
    visit(root.key)
    visit_inorder_reversed(root.left, visit)


def visit_preorder(root: Node, visit: Callable):
    """Префиксный обход.
    1. Элемент
    2. Левое поддерево
    3. Правое поддерево

    Применение:
        1. Копирование дерева
    """
    if not root:
        return None

    visit(root.key)
    visit_preorder(root.left, visit)
    visit_preorder(root.right, visit)


def visit_postorder(root: Node, visit: Callable):
    """Постфиксный обход.
    1. Левое поддерево
    2. Правое поддерево
    3. Элемент

    Применение:
        1. Корректное удаление элементов дерева
    """
    if not root:
        return None

    visit_postorder(root.left, visit)
    visit_postorder(root.right, visit)
    visit(root.key)
