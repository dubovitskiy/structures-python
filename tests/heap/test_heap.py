from structures.linear.binary_heap import Heap


def parent_index(index):
    return int((index - 1) / 2)


def test_init(heap: Heap):
    assert heap.current_size == 0
    assert len(heap.heap_list) == 0


def test_insert(heap: Heap):
    heap.insert(20)

    assert heap.current_size == 1
    assert len(heap.heap_list) == 1

    heap.insert(40)

    assert heap.current_size == 2
    assert len(heap.heap_list) == 2

    heap.insert(4)

    assert heap.current_size == 3
    assert len(heap.heap_list) == 3

    assert heap.heap_list == [4, 40, 20]
