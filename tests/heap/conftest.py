import pytest

from structures.linear.binary_heap import Heap


@pytest.fixture(scope='function')
def heap():
    return Heap()
