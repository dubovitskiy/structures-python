import pytest

# Some import magic
# noinspection PyUnresolvedReferences
from utils import ListVisitor, parametrize

import structures.linear.singly_linked_list.iterative as iteratively
import structures.linear.singly_linked_list.recursive as recursively
from structures.linear.singly_linked_list.structure import Container


def test_create_and_insert_at_head():
    head = iteratively.insert_at_head(None, Container(10))
    assert head.data == 10
    assert head.next is None

    head = iteratively.insert_at_head(head, 11)
    assert head.data == 11
    assert head.next.data == 10


@parametrize('insert_at_tail', [
    recursively.insert_at_tail,
    iteratively.insert_at_tail,
])
def test_create_and_insert_at_tail(insert_at_tail):
    head = insert_at_tail(None, Container(10))
    assert head.data == 10
    assert head.next is None

    head = insert_at_tail(head, 11)
    assert head.data == 10
    assert head.next.data == 11


def test_insert_at():
    head = iteratively.insert_at(None, 0, index=0)
    assert head.data == 0  # new head
    assert head.next is None

    head = iteratively.insert_at(head, 1, index=0)
    assert head.data == 1
    assert head.next.data == 0  # former head

    with pytest.raises(IndexError):
        iteratively.insert_at(head, 2, index=3)

    head = iteratively.insert_at(head, 2, index=2)
    assert head.data == 1
    assert head.next.data == 0
    assert head.next.next.data == 2

    head = iteratively.insert_at(head, 3, index=2)
    assert head.data == 1
    assert head.next.data == 0
    assert head.next.next.data == 3
    assert head.next.next.next.data == 2


@parametrize('traverse', [
    iteratively.traverse,
    recursively.traverse,
])
def test_traverse(traverse):
    visitor = ListVisitor()

    traverse(None, visitor)
    assert visitor.result == []

    head = iteratively.insert_at_head(None, 5)
    head = iteratively.insert_at_head(head, 4)
    head = iteratively.insert_at_head(head, 3)
    head = iteratively.insert_at_head(head, 2)
    head = iteratively.insert_at_head(head, 1)

    traverse(head, visitor)
    assert visitor.result == [1, 2, 3, 4, 5]


@parametrize('delete_at', [
    iteratively.delete_at,
])
def test_delete_at(delete_at):
    head = iteratively.insert_at_head(None, 5)
    head = iteratively.insert_at_head(head, 4)
    head = iteratively.insert_at_head(head, 3)
    head = iteratively.insert_at_head(head, 2)
    head = iteratively.insert_at_head(head, 1)

    head = delete_at(head, 2)

    visitor = ListVisitor()
    recursively.traverse(head, visitor)
    assert visitor.result == [1, 2, 4, 5]
    visitor.reset()

    head = delete_at(head, 3)
    recursively.traverse(head, visitor)
    assert visitor.result == [1, 2, 4]
    visitor.reset()

    head = delete_at(head, 0)
    recursively.traverse(head, visitor)
    assert visitor.result == [2, 4]


@parametrize('visit_reversed', [
    recursively.visit_reversed,
])
def test_visit_reversed(visit_reversed):
    head = iteratively.insert_at_head(None, 5)
    head = iteratively.insert_at_head(head, 4)
    head = iteratively.insert_at_head(head, 3)
    head = iteratively.insert_at_head(head, 2)
    head = iteratively.insert_at_head(head, 1)

    visitor = ListVisitor()
    visit_reversed(head, visitor)
    assert visitor.result == [5, 4, 3, 2, 1]


@parametrize('reverse', [
    iteratively.reverse,
    recursively.reverse,
])
def test_reverse(reverse):
    head = reverse(None)
    visitor = ListVisitor()
    iteratively.traverse(head, visitor)
    assert visitor.result == []

    head = iteratively.insert_at_head(None, 5)
    head = iteratively.insert_at_head(head, 4)
    head = iteratively.insert_at_head(head, 3)
    head = iteratively.insert_at_head(head, 2)
    head = iteratively.insert_at_head(head, 1)

    head = reverse(head)
    visitor = ListVisitor()
    iteratively.traverse(head, visitor)
    assert visitor.result == [5, 4, 3, 2, 1]


@parametrize('are_equal', [
    iteratively.are_equal,
    recursively.are_equal,
])
def test_are_equal(are_equal):
    assert are_equal(None, None)

    head_1 = iteratively.insert_at_head(None, 5)
    head_1 = iteratively.insert_at_head(head_1, 4)
    head_1 = iteratively.insert_at_head(head_1, 3)
    head_1 = iteratively.insert_at_head(head_1, 2)
    head_1 = iteratively.insert_at_head(head_1, 1)

    head_2 = iteratively.insert_at_head(None, 5)
    head_2 = iteratively.insert_at_head(head_2, 4)
    head_2 = iteratively.insert_at_head(head_2, 3)
    head_2 = iteratively.insert_at_head(head_2, 2)

    assert not are_equal(None, head_1)
    assert not are_equal(head_1, None)
    assert not are_equal(head_1, head_2)

    head_2 = iteratively.insert_at_head(head_2, 1)

    assert are_equal(head_1, head_2)
