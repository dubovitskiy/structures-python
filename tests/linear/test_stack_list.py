import pytest

from structures.linear.stack_list import Node, Stack
from structures.linear.stack_list import StackUnderflow


def test_init():
    stack_list = Stack()
    assert stack_list.head is None
    assert len(stack_list) == 0


def test_node_init():
    n = Node(0)
    assert n.next is None
    assert n.key == 0


def test_push():
    stack_list = Stack()
    stack_list.push(1)
    assert stack_list.head.key == 1
    assert len(stack_list) == 1

    stack_list.push(2)
    assert stack_list.head.key == 2
    assert len(stack_list) == 2


def test_peek_empty():
    stack_list = Stack()
    with pytest.raises(StackUnderflow):
        stack_list.peek()


def test_peek():
    stack_list = Stack()
    stack_list.push(1)
    assert stack_list.peek() == 1
    assert len(stack_list) == 1
    assert stack_list.head.key == 1


def test_pop_empty():
    stack_list = Stack()
    with pytest.raises(StackUnderflow):
        stack_list.pop()


def test_pop():
    stack_list = Stack()
    stack_list.push(1)
    stack_list.push(10)
    assert stack_list.pop() == 10
    assert len(stack_list) == 1
    assert stack_list.head.key == 1


def test_is_empty():
    stack_list = Stack()
    assert stack_list.is_empty
    stack_list.push(1)
    assert not stack_list.is_empty
    stack_list.pop()
    assert stack_list.is_empty
