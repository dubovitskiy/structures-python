import pytest

from structures.linear.stack_array import StackUnderflow
from structures.linear.stack_array import StackOverflow
from structures.linear.stack_array import Stack


def test_create():
    s = Stack(10)
    assert s._length == 0
    assert s._items == []
    assert s._size == 10


def test_is_full():
    s = Stack(0)
    assert s.is_full


def test_push_full():
    s = Stack(0)
    with pytest.raises(StackOverflow):
        s.push(1)


def test_push():
    s = Stack(1)
    s.push(100)
    assert len(s) == 1
    assert s.pop() == 100
    assert len(s) == 0


def test_peek():
    s = Stack(1)
    s.push(100)
    assert s.peek() == 100
    assert len(s) == 1


def test_pop_empty():
    s = Stack(10)
    with pytest.raises(StackUnderflow):
        s.pop()


def test_pop():
    s = Stack(3)
    s.push(1)
    assert s.pop() == 1
