import pytest

from structures.linear.queue_stacks import Queue
from structures.linear.queue_stacks import QueueFull
from structures.linear.queue_stacks import QueueEmpty


def test_queue_create():
    q = Queue(5)
    assert q._size == 5

    assert q._left_stack is not None
    assert q._left_stack.size == 2

    assert q._right_stack is not None
    assert q._right_stack.size == 3


def test_enqueue():
    q = Queue(2)
    assert q.is_empty
    q.enqueue('elem')
    assert not q.is_empty
    assert len(q) == 1

    assert q.dequeue() == 'elem'
    assert q.is_empty
    assert len(q) == 0


def test_enqueue_full():
    q = Queue(2)
    q.enqueue(1)
    q.enqueue(1)
    with pytest.raises(QueueFull):
        q.enqueue(1)


def test_dequeue_empty():
    q = Queue(3)
    with pytest.raises(QueueEmpty):
        q.dequeue()


def test_dequeue():
    q = Queue(4)
    q.enqueue(1)
    q.enqueue(2)
    q.enqueue(3)
    q.enqueue(4)
    assert q.dequeue() == 1
    assert q.dequeue() == 2
    assert q.dequeue() == 3
    assert q.dequeue() == 4


def test_is_empty():
    q = Queue(2)
    assert q.is_empty
    q.enqueue(1)
    assert not q.is_empty


def test_is_full():
    q = Queue(2)
    assert not q.is_full
    q.enqueue(1)
    q.enqueue(1)
    assert q.is_full
