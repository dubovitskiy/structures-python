# Some import magic
# noinspection PyUnresolvedReferences
from utils import ListVisitor, parametrize

import structures.recursive.bst.iterative as iteratively
import structures.recursive.bst.recursive as recursively
from structures.recursive.bst.structure import Node


def test_node_create():
    n = Node(1)
    assert n.key == 1
    assert n.left is None
    assert n.right is None


def test_node_repr():
    n = Node(100)
    assert repr(n) == f'<Node(key={n.key})>'


@parametrize('insert_node', [
    recursively.insert_node,
])
def test_insert(insert_node):
    root = Node(50)

    # Insert left
    insert_node(root, 49)
    assert root.left.key == 49

    # Insert right
    insert_node(root, 51)
    assert root.right.key == 51

    insert_node(root, 48)
    assert root.left.left.key == 48

    insert_node(root, 52)
    assert root.right.right.key == 52


@parametrize('count_nodes', [
    recursively.count_nodes,
])
def test_count_nodes(count_nodes):
    root = Node(50)

    assert count_nodes(root) == 1

    recursively.insert_node(root, 40)
    recursively.insert_node(root, 60)

    assert count_nodes(root) == 3


@parametrize('compute_max_depth', [
    recursively.compute_max_depth,
])
def test_compute_max_depth(compute_max_depth):
    root = Node(45)

    assert compute_max_depth(root) == 1

    recursively.insert_node(root, 1)

    assert compute_max_depth(root) == 2
    recursively.insert_node(root, 2)
    recursively.insert_node(root, 3)

    assert compute_max_depth(root) == 4


@parametrize('has_value', [
    recursively.has_value,
    iteratively.has_value,
])
def test_has_value(has_value, maketree):
    tree = maketree(10, 20)

    assert not has_value(tree, 22)
    assert has_value(tree, 15)


@parametrize('compute_min_value', [
    iteratively.compute_min_value,
    recursively.compute_min_value,
])
def test_compute_min_value(compute_min_value, maketree):
    root = maketree(30, 149)
    assert compute_min_value(root) == 30


@parametrize('compute_max_value', [
    iteratively.compute_max_value,
    recursively.compute_max_value,
])
def test_compute_max_value(compute_max_value, maketree):
    root = maketree(30, 149)
    assert compute_max_value(root) == 149


@parametrize('visit_increasing', [
    recursively.visit_inorder,
    iteratively.visit_inorder,
    iteratively.visit_inorder_without_stack,
])
def test_visit_increasing(visit_increasing, maketree):
    tree = maketree(1, 10)
    visitor = ListVisitor()
    visit_increasing(tree, visitor)
    assert visitor.result == [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


@parametrize('visit_preorder', [
    recursively.visit_preorder,
    iteratively.visit_preorder,
])
def test_visit_preorder(visit_preorder):
    tree = Node(5)
    recursively.insert_node(tree, 4)
    recursively.insert_node(tree, 6)
    recursively.insert_node(tree, 3)
    recursively.insert_node(tree, 7)

    visitor = ListVisitor()
    visit_preorder(tree, visitor)
    assert visitor.result == [5, 4, 3, 6, 7]


@parametrize('visit_decreasing', [
    recursively.visit_inorder_reversed,
    iteratively.visit_inorder_reversed,
])
def test_visit_decreasing(visit_decreasing, maketree):
    tree = maketree(1, 10)
    visitor = ListVisitor()
    visit_decreasing(tree, visitor)
    assert visitor.result == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]


@parametrize('check_is_bst', [
    recursively.check_is_bst,
    iteratively.check_is_bst,
    iteratively.check_is_bst_inorder_traversal,
])
def test_is_bst(check_is_bst, maketree):
    tree = maketree(1, 20)

    assert check_is_bst(tree)

    root = Node(30)
    root.left = Node(31)
    root.right = Node(20)

    assert not check_is_bst(root)
