import random

import pytest

from structures.recursive.bst.structure import Node
from structures.recursive.bst.recursive import insert_node


def make_tree(start: int, end: int):
    numbers = list(range(start, end + 1))
    random.shuffle(numbers)

    root_data = random.choice(numbers)
    numbers.remove(root_data)

    root = Node(root_data)

    for i in numbers:
        insert_node(root, i)

    return root


@pytest.fixture(scope='session')
def maketree():
    """Create tree from shuffled singly_linked_list."""
    return make_tree
