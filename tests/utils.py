import pytest
from types import FunctionType
from typing import List


def func_id(func: FunctionType) -> str:
    module = func.__module__.split('.')[-1]
    return f'{module} -> {func.__name__}'


def parametrize(name: str, func_list: List[FunctionType]):
    def decorator(func):
        marker = pytest.mark.parametrize(
            argnames=name,
            argvalues=func_list,
            ids=func_id
        )
        return marker(func)

    return decorator


class ListVisitor:
    def __init__(self):
        self._storage = []

    def __call__(self, value):
        self._storage.append(value)

    @property
    def result(self):
        return self._storage

    def reset(self):
        self._storage = []
