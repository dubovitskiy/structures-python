from structures.linear.linked_list import ListNode
from structures.linear.linked_list import LinkedList


def test_create_node():
    n = ListNode(10)
    assert n.key == 10
    assert n.next is None


def test_linked_list_node():
    node = ListNode(1)
    assert node.key == 1
    assert node.next is None


def test_linked_list():
    lst = LinkedList()
    assert lst.head is None
    assert lst.length == 0
    assert len(lst) == 0


def test_insert():
    lst = LinkedList()
    lst.insert(1)
    assert lst.head.key == 1
    assert len(lst) == 1

    lst.insert(200)
    assert lst.head.key == 200
    assert len(lst) == 2


def test_list_empty():
    lst = LinkedList()
    assert lst.is_empty

    lst.insert(1)

    assert not lst.is_empty


def test_find_previous():
    lst = LinkedList()
    lst.insert(4)
    lst.insert(100)
    lst.insert(20)
    lst.insert(40)
    lst.insert(75)

    prev = lst._find_prev(100)
    assert prev.key == 20


def test_find():
    lst = LinkedList()
    lst.insert(323)
    lst.insert(45)
    lst.insert(100)
    lst.insert(15)
    lst.insert(8)

    assert lst.find(45)
    assert 45 in lst

    assert not lst.find(101)
    assert 101 not in lst


def test_remove():
    lst = LinkedList()
    lst.insert(48)
    lst.insert(5)
    lst.insert(334)
    lst.insert(103)
    lst.insert(44)

    assert not lst.remove(100)
    assert lst.remove(334)
    assert 334 not in lst
